<?php
include "../_includes/header.php";
?>

<div id="about">
	<h1>About</h1>
	<p> I am Thomas Kamermans, a 20 years old media technology student at the Rotterdam University of Applied Science. This is a creative program which involves development of web, mobile and games. </p>
	<p>With this course I want to create cool and innovative projects. Therefore I put as many time and effort in these projects as possible. In my spare time I try to create as much creative stuff as possible from all the facets of the industry. With these efforts I am constantly trying to achieve higher and better goals. Therefore it has been very satisfying to see that I improved a lot these years.</p>
	<p>Aside from developing these projects I spend my time with some hobbies. One of these is photography. With my Nikon and basic photoshop skills I try to further then regular people with a camera, and I'm hoping this is visible in my pictures. I'm also interested in different sorts of media, like movies, music and games. I get a lot of inspiration from these sources, which have a lot of influence on my projects.</p>
	<p>I am eager to see what the future brings..</p>
</div>

<div id="contact">
	<h1>Contact</h1>
	<p>
		Mail me at <a class="importantLink" href="mailto:t.kamermans@gmail.com" target="_blank">t.kamermans@gmail.com</a><br />
		My <a class="importantLink" href="../_misc/cv_thomas_kamermans.pdf">resume</a> can be found here</br>
		I can also be found on <a href="http://www.facebook.com/thomas.kamermans">Facebook</a>, <a href="https://twitter.com/iamOscarMike">Twitter</a> and <a href="http://www.flickr.com/photos/thomaskamermans/">Flickr</a>
	</p>
</div>

<?php
include "../_includes/footer.php";
?>
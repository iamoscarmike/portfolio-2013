</div>
<div id="footer">
	<div class="line"></div>
	<span class="footertext">&#169 <?php echo date("Y");?> Thomas Kamermans</span>
	<a class="linkSocial" href="http://www.facebook.com/thomas.kamermans">Facebook</a>
	<a class="linkSocial" href="https://twitter.com/iamOscarMike">Twitter</a>
	<a class="linkSocial" href="http://www.flickr.com/photos/thomaskamermans/">Flickr</a>
</div>
</body>
</html>
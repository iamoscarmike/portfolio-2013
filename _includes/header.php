<!DOCTYPE html>
<html>
<head>
	<title>Thomas Kamermans - Portfolio</title>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script type="text/javascript" src="../fancybox/jquery.fancybox-1.3.4.js"></script>
	<script type="text/javascript" src="../fancybox/jquery.easing-1.3.pack.js"></script>
	<script type="text/javascript" src="../fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
	<script type="text/javascript" src="../_script/script.js"></script>
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-30354646-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
	<script src='../js/jqueryCookieGuard.1.0.min.js'></script>
	<script>
	  $(document).ready(function(){
	    $.cookieguard();
	    $.cookieguard.cookies.add('Facebook', 'datr,lu,c_user,xs', 'Facebook uses cookies to track the sites you visit', false);
	    $.cookieguard.cookies.add('Google Analytics', '__utma,__utmb,__utmc,__utmz,__utmv', 'These cookies are used to collect information about how visitors use our site. We use the information to compile reports and to help us improve the site. The cookies collect information in an anonymous form, including the number of visitors to the site, where visitors have come to the site from and the pages they visited.', false);
	    $.cookieguard.run();
	  });
	</script>
	<link rel="stylesheet" type="text/css" href="../_css/style.css"/>
	<link rel="stylesheet" type="text/css" href="../fancybox/jquery.fancybox-1.3.4.css"/>
	<link rel="icon" type="image/vnd.microsoft.icon" href="../_img/favicon.ico"/>
</head>
<body>
	<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
	<div class="banner">
		<a href="../home/"><img class="bannerLogo" src="../_img/banner.png" alt="logo"/></a>
		<img class="bannerStayTuned" src="../_img/staytuned.gif" alt="stay tuned"/>
	</div>
	<div class="line"></div>
	<div id="links">
		<a class="linkCategory" href="../home/">Portfolio</a>
		<a class="linkCategory" href="../about/">About me</a>
		<a class="linkSocial" href="http://www.facebook.com/thomas.kamermans">Facebook</a>
		<a class="linkSocial" href="https://twitter.com/iamOscarMike">Twitter</a>
		<a class="linkSocial" href="http://www.flickr.com/photos/thomaskamermans/">Flickr</a>
	</div>
	<div id="content">
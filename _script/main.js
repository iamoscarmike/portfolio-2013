function Game()
{
	var _canvas = document.getElementById('canvas');
	var _canvasContext = null;
		_canvasContext = _canvas.getContext('2d');
	
	var _canvasZoom = document.createElement("canvas");
		_canvasZoom.width = _canvas.width;
		_canvasZoom.height = _canvas.height;
	var _canvasZoomContext = null;
		_canvasZoomContext = _canvasZoom.getContext('2d');
			
	var _canvasLevel = document.createElement("canvas");
		_canvasLevel.width = _canvas.width;
		_canvasLevel.height = _canvas.height;
	var _canvasLevelContext = null;
		_canvasLevelContext = _canvasLevel.getContext('2d');
	
	var _canvasPlayer = document.createElement("canvas");
		_canvasPlayer.width = _canvas.width;
		_canvasPlayer.height = _canvas.height;
	var _canvasPlayerContext = null;
		_canvasPlayerContext = _canvasPlayer.getContext('2d');
	
	var Enviroment = new Image();
		Enviroment.src = "../img/enviroment.png";
	var Player = new Image();
		Player.src = "../img/yeti_animations.png";
	
	var levelWidth = 960;//canvas.width;
	var levelHeight = 640;//canvas.height;
	
	var yetiYIndex = 0;
	var yetiXIndex = 0;
	var yetiJumpIndex = 0;
	var lastDirection;
	var lastJumping;
	var Direction = "right";
	var referenceDate = new Date();
	var interval;
	var x;
	var y;
	var xSpeed = 5;
	var ySpeed = 0;
	var gravity = 1;
	var xPos = (canvas.width/2);
	var yPos = (canvas.height/2);
	var xLevel = -xPos;
	var yLevel = -xPos;
	var key = {left:false, right:false, up:false};
	var bitsize = 1 //bitsize 1:32 2:64 4:128 und so further
	
	
	this.Initialize = function()
	{
		
	}
	
	this.LoadContent = function()
	{	
		Enviroment.onload = function(){
			Player.onload = function(){
				if (_canvas && _canvas.getContext) {
					
					//Below function to draw level
					x = 0;
					y = 0;
					
					for(var i = 0; i <= levelArray.length; i ++)
					{
						_canvasLevelContext.drawImage(Enviroment, levelArray[i]*32, 0, 32, 32, x, y, 32, 32);
						x += 32;
						
						if(x>=levelWidth){
							y += 32;
							x = 0; 
						}
					}
				}
				_canvasPlayerContext.drawImage(Player, 0, 0, 64, 64, xPos, yPos, 64, 64);
				
				//_canvasContext.drawImage(_canvasLevel, xLevel+(levelWidth/2), yLevel+(levelHeight/2));
				//_canvasContext.drawImage(_canvasPlayer,0,0);
			}
		}
		this.GameLoop = setInterval(this.RunGameLoop, 0);
	}
	
	
	this.RunGameLoop = function (game)
	{
		myDate = new Date();
		interval = myDate-referenceDate;
		
		if (interval = 40)
		{
			
			document.portfolioGame.Update();
			document.portfolioGame.Draw();
		}
		referenceDate = myDate;
	}
	
	this.Update = function()
	{
		if(lastDirection == xPos)
		{
			if(Direction == "right")
			{
				yetiXIndex = 0;
				yetiYIndex = 0;
			}
			else if(Direction == "left")
			{
				yetiYIndex = 1;
				yetiXIndex = 0;
			}
		}
		lastDirection = xPos;
		lastJumping = yPos;
		
		document.onkeydown = keyCheckDown;
		document.onkeyup = keyCheckUp;
		
		function keyCheckDown()
		{
			var keyID = event.keyCode
			switch(keyID)
			{
				case 87:
				key.up = true;
				break;
				
				case 65:
				key.left = true;
				break;
				
				case 68:
				key.right = true;
				break;
			}
		}
		function keyCheckUp()
		{
			var keyID = event.keyCode
			switch(keyID)
			{
				case 87:
				key.up = false;
				break;
				
				case 65:
				key.left = false;
				break;
				
				case 68:
				key.right = false;
				break;
			}
		}
		/*
		//check input keys
		$(document).keydown(function(event){
			if (event.keyCode == '87') { //W
				key.up = true;
			}
			if(event.keyCode == '65'){ //A
				key.left = true;
			}
			if(event.keyCode == '68'){ //D
				key.right = true;
			}
		})
		$(document).keyup(function(event){
			if (event.keyCode == '87') { //W
				key.up = false;
			}
			if(event.keyCode == '65'){ //A
				key.left = false;
			}
			if(event.keyCode == '68'){ //D
				key.right = false;
			}
		})
		*/
		//right key pressed
		if(key.right)
		{
			if(
			levelArray[parseInt(yPos/32)*30+parseInt(xPos/32) + 2] != 0 ||
			levelArray[parseInt((yPos+32)/32)*30+parseInt(xPos/32) + 2] !=0 ||
			levelArray[parseInt((yPos+63)/32)*30+parseInt(xPos/32) + 2] !=0
			){
				PixDif = xPos%(parseInt(xPos/(32))*32);
				if(PixDif <= xSpeed){
					xPos -= PixDif;
				}
			}
			else{xPos += xSpeed;}
			
			//animations
			yetiYIndex = 2;
			yetiXIndex ++;
			if(yetiXIndex > 24){yetiXIndex = 0;}

			Direction = "right";
		}
		//left key pressed
		else if(key.left)
		{
			if(
			levelArray[parseInt(yPos/32)*30+parseInt((xPos-32)/32)] != 0 ||
			levelArray[parseInt((yPos+32)/32)*30+parseInt((xPos-32)/32)] != 0 ||
			levelArray[parseInt((yPos+63)/32)*30+parseInt((xPos-32)/32)] != 0
			){
				PixDif = (xPos%(parseInt(xPos/(32))*32));
				if(PixDif <= xSpeed){
					xPos -= PixDif;
				}
				else{xPos -= xSpeed;}
			}
			else{xPos -= xSpeed;}
			
			//animations
			yetiYIndex = 3;
			yetiXIndex ++;
			if(yetiXIndex > 24){yetiXIndex = 0;}
			
			Direction = "left";
		}
		
		//check floor		
		if(
		levelArray[parseInt((yPos+64)/32)*30+parseInt(xPos/32)] != 0 ||
		levelArray[parseInt((yPos+64)/32)*30+parseInt((xPos+32)/32)] != 0 ||
		levelArray[parseInt((yPos+64)/32)*30+parseInt((xPos+63)/32)] != 0
		){
			yetiJumpIndex = 0;
			PixDif = yPos%(parseInt(yPos/(32))*32);
			if(PixDif <= (16)){
				yPos -= (PixDif);
			}
			ySpeed = 0;
			if(key.up)
			{
				ySpeed = -16;
			}
		}
		else if(ySpeed < 12){
			ySpeed += gravity;
		}
		
		if(ySpeed >= 16){ySpeed = 16;}
		if(ySpeed <= -16){ySpeed = -16;}
		
		if(
		levelArray[(parseInt((yPos)/(32)))*30+(parseInt(xPos/(32)))] != 0 ||
		levelArray[(parseInt((yPos)/(32)))*30+(parseInt((xPos+32)/(32)))] != 0 ||
		levelArray[(parseInt((yPos)/(32)))*30+(parseInt((xPos+63)/(32)))] != 0 
		){
			PixDif = yPos%(parseInt(yPos/(32))*32);
			if(PixDif <= 16){
				yPos += PixDif
				if(ySpeed == 0){
					ySpeed = (gravity);
				}
				else{
					ySpeed = -(0.5*ySpeed);			
				}
			}
		}
		yPos += (ySpeed);
		if(lastJumping != yPos){
			if(yetiYIndex == 3 || yetiYIndex == 1)
			{
				yetiYIndex = 5;
			}
			else if(yetiYIndex == 2 || yetiYIndex == 0)
			{
				yetiYIndex = 4;
			}
			if(yetiJumpIndex >= 4){}
			else{yetiJumpIndex ++;}
			yetiXIndex = yetiJumpIndex;
			console.log(yetiXIndex);
		}
		
		_canvasPlayerContext.clearRect(0, 0, canvas.width, canvas.height);
		_canvasPlayerContext.drawImage(Player, yetiXIndex*64, yetiYIndex*64, 64, 64, canvas.width/2, canvas.height/2, 64, 64);

	}
	
	this.Draw = function()
	{
		//_canvasLevel.clearRect(0, 0, canvas.width, canvas.height);

		x = 0;
		y = 0;
		
		xLevel = -xPos;
		yLevel = -yPos;
		
		_canvasZoomContext.fillRect(0, 0, canvas.width, canvas.height);
		_canvasZoomContext.drawImage(_canvasLevel, (xLevel+(levelWidth/2)), (yLevel+(levelHeight/2)),levelWidth*bitsize,levelHeight*bitsize);
		_canvasZoomContext.drawImage(_canvasPlayer,0,0);
		
		_canvasContext.drawImage(_canvasZoom,120,80,720,490,0,0,905,603);
		
	}
	
	this.run = function()
	{
		this.LoadContent();
		this.Initialize();
	}
}